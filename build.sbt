name := "marketing-analytics-task"

version := "0.1"

scalaVersion := "2.12.2"

resolvers += "Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.4.4",
  "org.apache.spark" %% "spark-sql" % "2.4.4",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "MrPowers" % "spark-fast-tests" % "0.20.0-s_2.12"
)

fork in Test := true
javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:+CMSClassUnloadingEnabled")
