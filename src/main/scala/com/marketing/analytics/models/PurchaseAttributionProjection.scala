package com.marketing.analytics.models

import java.sql.Timestamp

/**
 * Attribution projections (output of first task)
 *
 * @param purchaseId
 * @param purchaseTime
 * @param billingCost
 * @param isConfirmed
 * @param sessionId
 * @param campaignId
 * @param channelId
 */
case class PurchaseAttributionProjection(purchaseId: String,
                                         purchaseTime: Timestamp,
                                         billingCost: Double,
                                         isConfirmed: Boolean,
                                         sessionId: String,
                                         campaignId: String,
                                         channelId: String)
