package com.marketing.analytics.models

case class ChannelEngagement(campaignId: String, channelId: String, engagement: Long)
