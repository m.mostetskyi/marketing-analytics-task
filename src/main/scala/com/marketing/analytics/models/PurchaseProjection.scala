package com.marketing.analytics.models

import org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp

case class PurchaseProjection(purchaseId: String, purchaseTime: Timestamp, billingCost: Double, isConfirmed: Boolean)
