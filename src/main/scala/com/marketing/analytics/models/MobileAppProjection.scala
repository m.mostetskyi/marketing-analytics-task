package com.marketing.analytics.models

import java.sql.Timestamp

case class MobileAppProjection(userId: String,
                               eventId: String,
                               eventTime: Timestamp,
                               eventType: String,
                               attributes: String)
