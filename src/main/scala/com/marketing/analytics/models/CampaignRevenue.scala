package com.marketing.analytics.models

case class CampaignRevenue(campaignId: String, revenue: Double)
