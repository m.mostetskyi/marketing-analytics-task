package com.marketing.analytics

import org.apache.spark.sql.SparkSession

object SparkSessionProvider {

  lazy val sparkSession: SparkSession = {
    SparkSession
      .builder()
      .getOrCreate()
  }

}
