package com.marketing.analytics.udaf

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types.{DataType, DoubleType, LongType, StringType, StructField, StructType}

class SessionGenerator extends UserDefinedAggregateFunction {

  override def inputSchema: StructType =
    StructType(
      StructField("userId", StringType) ::
        StructField("value", LongType) :: Nil
    )

  override def bufferSchema: StructType = StructType(
    StructField("userId", StringType) ::
      StructField("count", LongType) :: Nil
  )

  override def dataType: DataType = StringType

  override def deterministic: Boolean = true

  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    buffer(0) = ""
    buffer(1) = 0L
  }

  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    buffer(0) = input.getAs[String](0)
    buffer(1) = input.getAs[Long](1)
  }

  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    buffer1(0) = buffer1.getAs[String](0) + "_" + buffer2.getAs[Long](0)
    buffer1(1) = buffer1.getAs[Long](1) + buffer2.getAs[Long](1)
  }

  override def evaluate(buffer: Row): Any = {
    "session_"+ buffer.getString(0) + "_" + buffer.getLong(1)
  }
}
