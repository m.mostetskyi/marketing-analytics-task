package com.marketing.analytics.services

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession

/**
 * This class responsible for registering input data for further analysis.
 *
 * @param sparkSession
 */
class AnalyticsDataRegister(sparkSession: SparkSession) {

  @transient lazy private val logger: Logger = Logger.getLogger(getClass.getName)

  def registerMobileAppData(location: String): Unit = {
    logger.debug("Registering 'mobile_app_projection'...")

    sparkSession.read.option("header", "true")
      .option("escape", "\"")
      .csv(location)
      .createOrReplaceTempView("mobile_data")

    logger.debug("'mobile_app_projection' successfully registered")
  }

  def registerPurchaseProjection(location: String): Unit = {
    logger.debug("Registering 'purchase_projection'...")

    sparkSession.read.option("header", "true")
      .option("escape", "\"")
      .option("inferSchema", "true")
      .csv(location)
      .createOrReplaceTempView("purchase_projection")

    logger.debug("'purchase_projection' successfully registered")
  }

}
