package com.marketing.analytics.services

import com.marketing.analytics.models.{CampaignRevenue, ChannelEngagement}
import org.apache.spark.sql.Dataset

trait StatisticsService {

  /**
   * Get top N campaigns based on revenue
   *
   * @param n number of campaigns
   * @return dataset with campaigns info
   */
  def getTopNCampaigns(n: Int): Dataset[CampaignRevenue]

  /**
   * Get top channel for each campaign based on engagement
   *
   * @return dataset with channels info
   */
  def getTopChannelPerCampaign: Dataset[ChannelEngagement]

}
