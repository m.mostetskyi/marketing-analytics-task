package com.marketing.analytics.services

trait PurchaseService {

  /**
   * This service responsible for registering table, based on users sessions and other data.
   *
   * Implementation will register in-memory table, which will be used for further analysis.
   */
  def registerPurchaseAttributionProjection(): Unit

}
