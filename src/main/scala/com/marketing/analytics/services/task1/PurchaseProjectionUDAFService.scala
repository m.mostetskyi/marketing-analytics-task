package com.marketing.analytics.services.task1

import com.marketing.analytics.models.PurchaseAttributionProjection
import com.marketing.analytics.services.PurchaseService
import com.marketing.analytics.udaf.SessionGenerator
import org.apache.log4j.Logger
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, Encoders, Row, SparkSession}


class PurchaseProjectionUDAFService(sparkSession: SparkSession) extends PurchaseService {

  @transient lazy private val logger: Logger = Logger.getLogger(getClass.getName)

  private val attributionProjectionEncoder = Encoders.product[PurchaseAttributionProjection]

  def registerPurchaseAttributionProjection_v2(): Unit = {
    val userIdSpec = Window.partitionBy("userId").orderBy("userId")
    val sessionIdSpec = Window.partitionBy("sessionId").orderBy("userId")
    val userIdBoundSpec = Window.partitionBy("userId").rowsBetween(Window.unboundedPreceding, Window.currentRow)


    val attributionProjection = sparkSession.table("mobile_data")
      .withColumn("temp", when(lag(col("eventType"), 1).over(userIdSpec) === "app_close", lit(1)).otherwise(lit(0)))
      .withColumn("temp1", sum("temp").over(userIdBoundSpec))
      .groupBy("userId", "temp1")
      .agg(concat(col("userId"), lit("_"), col("temp1")).alias("sessionId"),
        collect_list(col("attributes")).alias("attributes"))
      .drop("temp1")
      .select(col("*"), explode(col("attributes")).alias("expl_attr"))
      .withColumn("campaignId", get_json_object(col("expl_attr"), "$.campaign_id"))
      .withColumn("channelId", get_json_object(col("expl_attr"), "$.channel_id"))
      .withColumn("purchaseId", get_json_object(col("expl_attr"), "$.purchase_id"))
      .drop("attributes", "expl_attr")
      .orderBy("userId")
      .withColumn("lastCampaignId", last("campaignId", true) over (sessionIdSpec))
      .withColumn("lastChannelId", last("channelId", true) over (sessionIdSpec))
      .drop("campaignId", "channelId")
      .withColumnRenamed("lastCampaignId", "campaignId")
      .withColumnRenamed("lastChannelId", "channelId")
      .join(sparkSession.table("purchase_projection"), "purchaseId")
      .orderBy("purchaseId").as(attributionProjectionEncoder)

    attributionProjection.createOrReplaceTempView("attribution_projection")
  }

  def registerPurchaseAttributionProjection(): Unit = {
    val userIdSpec = Window.partitionBy("userId").orderBy("userId")
    val sessionIdSpec = Window.partitionBy("sessionId").orderBy("userId")
    val userIdBoundSpec = Window.partitionBy("userId").rowsBetween(Window.unboundedPreceding, Window.currentRow)

    val sg = new SessionGenerator

    val uniqueSessionData: Dataset[Row] = {
      sparkSession.table("mobile_data")
        .withColumn("temp", when(lag(col("eventType"), 1).over(userIdSpec) === "app_close", lit(1)).otherwise(lit(0)))
        .withColumn("temp1", sum("temp").over(userIdBoundSpec))
        .groupBy("userId", "temp1")
        .agg(sg(col("userId"), col("temp1")).alias("sessionId"),
          collect_list(col("attributes")).alias("attributes"))
        .drop("temp1")
    }

    val explodedAttributeColumn: Dataset[Row] = {
      uniqueSessionData
        .select(col("*"), explode(col("attributes")).alias("expld_attributes"))
        .withColumn("campaignId", get_json_object(col("expld_attributes"), "$.campaign_id"))
        .withColumn("channelId", get_json_object(col("expld_attributes"), "$.channel_id"))
        .withColumn("purchaseId", get_json_object(col("expld_attributes"), "$.purchase_id"))
        .drop("attributes", "expld_attributes")
        .orderBy("userId")
    }

    val filledEmptyAttributes: Dataset[Row] = {
      explodedAttributeColumn
        .withColumn("lastCampaignId", last("campaignId", true) over (sessionIdSpec))
        .withColumn("lastChannelId", last("channelId", true) over (sessionIdSpec))
        .drop("campaignId", "channelId")
        .withColumnRenamed("lastCampaignId", "campaignId")
        .withColumnRenamed("lastChannelId", "channelId")
    }

    val joinedPurchaseData: Dataset[PurchaseAttributionProjection] = {
      filledEmptyAttributes
        .join(sparkSession.table("purchase_projection"), "purchaseId")
        .select("purchaseId", "purchaseTime", "billingCost", "isConfirmed", "sessionId", "campaignId", "channelId")
        .orderBy("purchaseId").as(attributionProjectionEncoder)
    }
    joinedPurchaseData.createOrReplaceTempView("attribution_projection")
  }
}
