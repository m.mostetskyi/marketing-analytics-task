package com.marketing.analytics.services.task1

import com.marketing.analytics.models.PurchaseAttributionProjection
import com.marketing.analytics.services.PurchaseService
import org.apache.log4j.Logger
import org.apache.spark.sql.{Encoders, SparkSession}


class PurchaseProjectionSQLService(sparkSession: SparkSession) extends PurchaseService {

  @transient lazy private val logger: Logger = Logger.getLogger(getClass.getName)

  private val attributionProjectionEncoder = Encoders.product[PurchaseAttributionProjection]

  private val purchaseAttributionQuery: String = "SELECT pp.*, sessionId, last_campaign as campaignId, last_channel as channelId" +
    " FROM (SELECT *, LAST(campaignId, true) OVER (PARTITION BY sessionId ORDER BY userId) last_campaign," +
    " LAST(channelId, true) OVER (PARTITION BY sessionId ORDER BY userId) last_channel" +
    " FROM (SELECT *, CONCAT(userId, CONCAT('_', SUM(new_session) OVER (PARTITION BY userId ORDER BY eventTime))) AS sessionId" +
    " FROM (SELECT *, get_json_object(attributes, \"$.purchase_id\") purchaseId," +
    " get_json_object(attributes, \"$.campaign_id\") campaignId, get_json_object(attributes, \"$.channel_id\") channelId," +
    " CASE WHEN attributes IS NULL THEN 1 ELSE 0 END AS new_session FROM mobile_data WHERE eventType IN " +
    " ('app_open', 'purchase', 'app_close')) ORDER BY userId) WHERE attributes IS NOT NULL ORDER BY userId) md" +
    " JOIN purchase_projection pp ON md.purchaseId = pp.purchaseId" +
    " WHERE md.purchaseId is not null"

  def registerPurchaseAttributionProjection(): Unit = {

    logger.debug(s"Executing following query: $purchaseAttributionQuery")

    val attributionProjection = sparkSession.sql(purchaseAttributionQuery).as(attributionProjectionEncoder)

    attributionProjection.createOrReplaceTempView("attribution_projection")
  }

}
