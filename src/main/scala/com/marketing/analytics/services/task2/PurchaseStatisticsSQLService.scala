package com.marketing.analytics.services.task2

import com.marketing.analytics.models.{CampaignRevenue, ChannelEngagement}
import com.marketing.analytics.services.StatisticsService
import org.apache.log4j.Logger
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

/**
 * SQL implementation of purchases statistics (Task 2)
 *
 * @param sparkSession
 */
class PurchaseStatisticsSQLService(sparkSession: SparkSession) extends StatisticsService {

  @transient lazy private val logger: Logger = Logger.getLogger(getClass.getName)

  private val topNCampaignsQuery = "SELECT campaignId, sum(billingCost) as revenue FROM attribution_projection" +
    " WHERE isConfirmed == 'TRUE' GROUP BY campaignId ORDER BY sum(billingCost) DESC LIMIT "

  private val topChannelPerCampaignQuery = "SELECT campaignId, channelId, engagement FROM (SELECT campaignId, channelId, engagement," +
    " RANK() OVER (PARTITION BY campaignId ORDER BY engagement DESC) row FROM " +
    " (SELECT campaignId, channelId, (count(distinct sessionId)) engagement FROM attribution_projection GROUP BY campaignId, channelId " +
    " ORDER BY count(distinct sessionId) desc)) WHERE row = 1"

  override def getTopNCampaigns(n: Int): Dataset[CampaignRevenue] = {
    val campaignRevenueEncoder = Encoders.product[CampaignRevenue]
    logger.debug(s"Executing following query: $topNCampaignsQuery")
    sparkSession.sql(topNCampaignsQuery + n).as(campaignRevenueEncoder)
  }

  override def getTopChannelPerCampaign: Dataset[ChannelEngagement] = {
    val channelEngagementEncoder = Encoders.product[ChannelEngagement]
    logger.debug(s"Executing following query: $topChannelPerCampaignQuery")
    sparkSession.sql(topChannelPerCampaignQuery).as(channelEngagementEncoder)
  }
}
