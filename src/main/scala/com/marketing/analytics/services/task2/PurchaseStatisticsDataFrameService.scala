package com.marketing.analytics.services.task2

import com.marketing.analytics.models.{CampaignRevenue, ChannelEngagement}
import com.marketing.analytics.services.StatisticsService
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{countDistinct, desc, rank}
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

/**
 * DataFrame/Dataset API implementation of purchases statistics (Task 2)
 *
 * @param sparkSession
 */
class PurchaseStatisticsDataFrameService(sparkSession: SparkSession) extends StatisticsService {

  override def getTopNCampaigns(n: Int): Dataset[CampaignRevenue] = {
    val campaignRevenueEncoder = Encoders.product[CampaignRevenue]
    sparkSession.table("attribution_projection")
      .where("isConfirmed = TRUE")
      .groupBy("campaignId")
      .sum("billingCost")
      .orderBy(desc("sum(billingCost)")).limit(n)
      .withColumnRenamed("sum(billingCost)", "revenue")
      .as(campaignRevenueEncoder)
  }

  override def getTopChannelPerCampaign: Dataset[ChannelEngagement] = {
    val channelEngagementEncoder = Encoders.product[ChannelEngagement]
    val winSpec = Window.partitionBy("campaignId").orderBy(desc("engagement"))
    sparkSession.table("attribution_projection")
      .groupBy("campaignId", "channelId")
      .agg(countDistinct("sessionId").as("engagement"))
      .orderBy(desc("engagement"))
      .withColumn("row", rank() over (winSpec))
      .select("campaignId", "channelId", "engagement")
      .where("row = 1").as(channelEngagementEncoder)
  }
}
