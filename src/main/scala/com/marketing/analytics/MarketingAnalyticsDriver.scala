package com.marketing.analytics

import com.marketing.analytics.services.task1.PurchaseProjectionSQLService
import com.marketing.analytics.services.task2.{PurchaseStatisticsDataFrameService, PurchaseStatisticsSQLService}
import com.marketing.analytics.services.{AnalyticsDataRegister, StatisticsService}
import org.apache.log4j.Logger

object MarketingAnalyticsDriver {

  @transient lazy private val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {

    //Ex. s3://marketing-analytics/data/
    val dataRootPath = args(0)

    logger.info("Starting Spark session...")
    val sparkSession = SparkSessionProvider.sparkSession

    logger.info("Registering analytics data")
    val dataRegister = new AnalyticsDataRegister(sparkSession)
    dataRegister.registerMobileAppData(dataRootPath + "mobile_app_projection")
    dataRegister.registerPurchaseProjection(dataRootPath + "purchase_projection")

    logger.info("Building purchase attribution projection...")
    val purchaseProjectionService = new PurchaseProjectionSQLService(sparkSession)
    purchaseProjectionService.registerPurchaseAttributionProjection()

    logger.info("Calculate statistics (SQL implementation)")
    val sqlPurchaseStatistics: StatisticsService = new PurchaseStatisticsSQLService(sparkSession)
    sqlPurchaseStatistics.getTopNCampaigns(10)
    sqlPurchaseStatistics.getTopChannelPerCampaign

    logger.info("Calculate statistics (Dataset/Dataframe API implementation)")
    val datasetPurchaseStatistics: StatisticsService = new PurchaseStatisticsDataFrameService(sparkSession)
    datasetPurchaseStatistics.getTopNCampaigns(10)
    datasetPurchaseStatistics.getTopChannelPerCampaign

  }

}
