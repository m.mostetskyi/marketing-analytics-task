package com.marketing.analytics.utils

import com.marketing.analytics.SparkSessionTestWrapper
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{Dataset, Encoder, Encoders}

object SparkIOUtils extends SparkSessionTestWrapper{

  import spark.implicits._

  def readCSVDataset[T <: Product : Encoder](location: String): Dataset[T] = {
    spark.read
      .option("escape", "\"")
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(location)
      .as[T]
  }

  def readCSVDatasetWithSchema[T <: Product : Encoder](location: String, schema: StructType): Dataset[T] = {
    spark.read
      .option("escape", "\"")
      .option("header", "true")
      .schema(schema)
      .csv(location)
      .as[T]
  }

}
