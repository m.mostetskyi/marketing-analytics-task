package com.marketing.analytics.services.task2

import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import com.marketing.analytics.SparkSessionTestWrapper
import com.marketing.analytics.models.{CampaignRevenue, ChannelEngagement}
import com.marketing.analytics.services.task1.PurchaseProjectionSQLService
import com.marketing.analytics.services.{AnalyticsDataRegister, StatisticsService}
import com.marketing.analytics.utils.SparkIOUtils
import org.apache.spark.sql.Encoders
import org.scalatest.{BeforeAndAfter, FunSpec}

class StatisticsServiceSpec extends FunSpec with BeforeAndAfter with DataFrameComparer with SparkSessionTestWrapper {

  import spark.implicits._

  before {
    val dataRegister = new AnalyticsDataRegister(spark)
    dataRegister.registerMobileAppData("src/test/resources/mobile_app_projection.csv")
    dataRegister.registerPurchaseProjection("src/test/resources/purchase_projection.csv")

    val purchaseProjection = new PurchaseProjectionSQLService(spark)
    purchaseProjection.registerPurchaseAttributionProjection()

  }

  it("should return expected dataset for .getTopNCampaigns(n) (SQL implementation) ") {
    val statisticsService: StatisticsService = new PurchaseStatisticsSQLService(spark)

    val expectedDF = SparkIOUtils.readCSVDataset[CampaignRevenue]("src/test/resources/statistics.service/sql/expected_topN_campaigns.csv")

    val actualDF = statisticsService.getTopNCampaigns(10)
    assertSmallDatasetEquality(expectedDF, actualDF)
  }

  it("should return expected dataset for .getTopChannelPerCampaign (SQL implementation) ") {
    val statisticsService: StatisticsService = new PurchaseStatisticsSQLService(spark)

    val channelEngagementSchema = Encoders.product[ChannelEngagement].schema

    val expectedDF = SparkIOUtils.readCSVDatasetWithSchema[ChannelEngagement]("src/test/resources/statistics.service/sql/expected_top_channel.csv", channelEngagementSchema)

    val actualDF = statisticsService.getTopChannelPerCampaign
    assertSmallDatasetEquality(expectedDF, actualDF, ignoreNullable = true)
  }

  it("should return expected dataset for .getTopNCampaigns(n) (Dataset/DataFrame implementation) ") {
    val statisticsService: StatisticsService = new PurchaseStatisticsDataFrameService(spark)

    val expectedDF = SparkIOUtils.readCSVDataset[CampaignRevenue]("src/test/resources/statistics.service/dataset/expected_topN_campaigns.csv")

    val actualDF = statisticsService.getTopNCampaigns(10)
    assertSmallDatasetEquality(expectedDF, actualDF)
  }

  it("should return expected dataset for .getTopChannelPerCampaign (Dataset/DataFrame implementation) ") {
    val statisticsService: StatisticsService = new PurchaseStatisticsDataFrameService(spark)

    val channelEngagementSchema = Encoders.product[ChannelEngagement].schema

    val expectedDF = SparkIOUtils.readCSVDatasetWithSchema[ChannelEngagement]("src/test/resources/statistics.service/dataset/expected_top_channel.csv", channelEngagementSchema)

    val actualDF = statisticsService.getTopChannelPerCampaign
    assertSmallDatasetEquality(expectedDF, actualDF, ignoreNullable = true)
  }
}
