package com.marketing.analytics.services.task1

import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import com.marketing.analytics.SparkSessionTestWrapper
import com.marketing.analytics.models.PurchaseAttributionProjection
import com.marketing.analytics.services.{AnalyticsDataRegister, PurchaseService}
import com.marketing.analytics.utils.SparkIOUtils
import org.scalatest.{BeforeAndAfter, FunSpec}

class PurchaseProjectionServiceSpec extends FunSpec with BeforeAndAfter with DataFrameComparer with SparkSessionTestWrapper {

  import spark.implicits._

  var purchaseProjection: PurchaseService = _

  before {
    val dataRegister = new AnalyticsDataRegister(spark)
    dataRegister.registerMobileAppData("src/test/resources/mobile_app_projection.csv")
    dataRegister.registerPurchaseProjection("src/test/resources/purchase_projection.csv")

    spark.catalog.dropTempView("attribution_projection")

  }

  it("should return expected number of records from attribution projection (SQL implementation)") {

    purchaseProjection = new PurchaseProjectionSQLService(spark)
    purchaseProjection.registerPurchaseAttributionProjection()

    val rowsCount = spark.sql("SELECT * FROM attribution_projection").count()
    assert(rowsCount == 6)
  }

  it("should return expected dataset (SQL implementation)") {

    purchaseProjection = new PurchaseProjectionSQLService(spark)
    purchaseProjection.registerPurchaseAttributionProjection()

    val expectedDF = SparkIOUtils.readCSVDataset[PurchaseAttributionProjection]("src/test/resources/purchase.attribution.projection/task1/expected.csv")
    val actualDF = spark.sql("SELECT * FROM attribution_projection").as[PurchaseAttributionProjection]

    assertSmallDatasetEquality(expectedDF, actualDF)
  }

  it("should return expected number of records from attribution projection (UDAF implementation)") {

    purchaseProjection = new PurchaseProjectionUDAFService(spark)
    purchaseProjection.registerPurchaseAttributionProjection()

    val rowsCount = spark.sql("SELECT * FROM attribution_projection").count()
    assert(rowsCount == 6)
  }

  it("should return expected dataset (UDAF implementation)") {

    purchaseProjection = new PurchaseProjectionUDAFService(spark)
    purchaseProjection.registerPurchaseAttributionProjection()

    val expectedDF = SparkIOUtils.readCSVDataset[PurchaseAttributionProjection]("src/test/resources/purchase.attribution.projection/task2/expected.csv")
    val actualDF = spark.sql("SELECT * FROM attribution_projection").as[PurchaseAttributionProjection]

    assertSmallDatasetEquality(expectedDF, actualDF)
  }

}
