## Marketing analytics task

This project is responsible for analysing various marketing campaigns, made by marketing department.

### Project structure

Project has a following structure:
```bash
.
├── main
│   ├── resources
│   │   └── log4j.properties
│   └── scala
│       └── com
│           └── marketing
│               └── analytics
│                   ├── MarketingAnalyticsDriver.scala
│                   ├── SparkSessionProvider.scala
│                   ├── models
│                   │   ├── CampaignRevenue.scala
│                   │   ├── ChannelEngagement.scala
│                   │   ├── MobileAppProjection.scala
│                   │   ├── PurchaseAttributionProjection.scala
│                   │   └── PurchaseProjection.scala
│                   ├── services
│                   │   ├── AnalyticsDataRegister.scala
│                   │   ├── PurchaseService.scala
│                   │   ├── StatisticsService.scala
│                   │   ├── task1
│                   │   │   ├── PurchaseProjectionSQLService.scala
│                   │   │   └── PurchaseProjectionUDAFService.scala
│                   │   └── task2
│                   │       ├── PurchaseStatisticsDataFrameService.scala
│                   │       └── PurchaseStatisticsSQLService.scala
│                   └── udaf
│                       └── SessionGenerator.scala
└── test
    ├── resources
    │   ├── mobile_app_projection.csv
    │   ├── purchase.attribution.projection
    │   │   ├── task1
    │   │   │   └── expected.csv
    │   │   └── task2
    │   │       └── expected.csv
    │   ├── purchase_projection.csv
    │   └── statistics.service
    │       ├── dataset
    │       │   ├── expected_topN_campaigns.csv
    │       │   └── expected_top_channel.csv
    │       └── sql
    │           ├── expected_topN_campaigns.csv
    │           └── expected_top_channel.csv
    └── scala
        └── com
            └── marketing
                └── analytics
                    ├── SparkSessionTestWrapper.scala
                    ├── services
                    │   ├── task1
                    │   │   └── PurchaseProjectionServiceSpec.scala
                    │   └── task2
                    │       └── StatisticsServiceSpec.scala
                    └── utils
                        └── SparkIOUtils.scala

```

Package ``com.marketing.analytics.models`` contains Scala case classes needed for representing data structure.

Package ``com.marketing.analytics.services`` contains business logic of the marketing analytics divided by 2 subfolders (specify number of task) and also some interfaces.

Package ``com.marketing.analytics.udaf`` contains custom UDAF which was implemented for task 2.2.

The entry point of the project is [MarketingAnalyticsDriver.scala](src/main/scala/com/marketing/analytics/MarketingAnalyticsDriver.scala).
This Spark driver contains a workflow of job execution. For appropriate execution you need to specify path to the input data, for example:
```
s3://analytics-department/data/

or

/tmp/analytics-data/
```

Then driver will start execution. On the next step driver will register files: __mobile_app_projection__ and __purchase_projection__.

### Tasks description

#### Task #1. Build Purchases Attribution Projection

The projection is dedicated to enabling a subsequent analysis of marketing campaigns and channels. 
The target schema:
- purchaseId: String
- purchaseTime: Timestamp
- billingCost: Double
- isConfirmed: Boolean
- sessionId: String  // a session starts with app_open event and finishes with app_close 
- campaignId: String  // derived from app_open#attributes#campaign_id
- channelIid: String    // derived from app_open#attributes#channel_id

The implementation of logic above located in [PurchaseService.scala](src/main/scala/com/marketing/analytics/services/PurchaseService.scala).
```scala
  /**
   * This service responsible for registering table, based on users sessions and other data.
   *
   * Implementation will register in-memory table, which will be used for further analysis.
   */
  def registerPurchaseAttributionProjection(): Unit
```

This trait define a method, which should be implemented in child classes and register calculated table in-memory.

2 child classes was implemented:
- [PurchaseProjectionSQLService.scala](src/main/scala/com/marketing/analytics/services/task1/PurchaseProjectionSQLService.scala) - SQL implementation (task 2.1)
- [PurchaseProjectionUDAFService.scala](src/main/scala/com/marketing/analytics/services/task1/PurchaseProjectionUDAFService.scala) - DataFrame/Dataset API implementation (task 2.2)

Was defined a SQL query which uses some Window Functions and Subqueries to calculate the unique sessionId and also merge rows under the same sessionId, to collect
some additional information about purchaseId, campaignId and channelId. Also was used some functions to extract value from JSON string.
Ex.
```
get_json_object(attributes, "$.purchase_id")
```  

In the result was registered table ``attribution_projection``, which contains appropriate result.


| purchaseId | purchaseTime                  | billingCost | isConfirmed | sessionId | campaignId | channelId  | 
|------------|-------------------------------|-------------|-------------|-----------|------------|------------| 
| p1         | 2019-01-01T00:01:05.000+02:00 | 100.5       | true        | u1_0      | cmp1       | Google Ads | 
| p2         | 2019-01-01T00:03:10.000+02:00 | 200.0       | true        | u2_0      | cmp1       | Yandex Ads | 
| p3         | 2019-01-01T01:12:15.000+02:00 | 300.0       | false       | u3_1      | cmp1       | Google Ads | 
| p6         | 2019-01-02T13:03:00.000+02:00 | 99.0        | false       | u3_2      | cmp2       | Yandex Ads | 
| p4         | 2019-01-01T02:13:05.000+02:00 | 50.2        | true        | u3_3      | cmp2       | Yandex Ads | 
| p5         | 2019-01-01T02:15:05.000+02:00 | 75.0        | true        | u3_3      | cmp2       | Yandex Ads | 



#### Task #2. Calculate Marketing Campaigns And Channels Statistics 

Use the purchases-attribution projection to build aggregates that provide the following insights:

Task #2.1. Top Campaigns: 
- What are the Top 10 marketing campaigns that bring the biggest revenue (based on billingCost of confirmed purchases)?

Task #2.2. Channels engagement performance: 
- What is the most popular (i.e. Top) channel that drives the highest amount of unique sessions (engagements)  with the App in each campaign?

Requirements to the tasks #2:
Should be implemented by using plan SQL on top of Spark DataFrame API
Will be a plus: an additional alternative implementation of the same tasks by using Spark Scala DataFrame / Datasets  API only (without plain SQL)


To implement tasks above was created an [StatisticsService.scala](src/main/scala/com/marketing/analytics/services/StatisticsService.scala) trait which define logic of child.

```scala

  /**
   * Get top N campaigns based on revenue
   *
   * @param n number of campaigns
   * @return dataset with campaigns info
   */
  def getTopNCampaigns(n: Int): Dataset[CampaignRevenue]

  /**
   * Get top channel for each campaign based on engagement
   *
   * @return dataset with channels info
   */
  def getTopChannelPerCampaign: Dataset[ChannelEngagement]
```

Also, was developed 2 implementation:
- With plain SQL - [PurchaseStatisticsSqlService.scala](src/main/scala/com/marketing/analytics/services/task2/PurchaseStatisticsSQLService.scala)
- By using Datasets/Dataframe APIs - [PurchaseStatisticsDataFrameService.scala](src/main/scala/com/marketing/analytics/services/task2/PurchaseStatisticsDataFrameService.scala)

### Local development
You can set up local Spark cluster in standalone mode in docker. So, first of all you need to install docker and docker-compose.

#### Useful tips
- Start the cluster: `docker-compose up -d`
- Visit `http://localhost:8080` in the browser to see the WebUI
- Watch cluster logs: `docker-compose logs -f`
- Add more workers (e.g. up to 3): `docker-compose up -d --scale spark-worker=3`
- Watch cluster resource usage in real time: `docker stats`
- Shutdown the cluster and clean up: `docker-compose down`

#### Submit app to the cluster

```sh
# run app
docker exec spark-master bin/spark-submit --master spark://spark-master:7077 --class com.marketing.analytics.MarketingAnalyticsDriver target/scala-2.12/marketing-analytics-task_2.12-0.1.jar s3://analytics-department/data/

```
